//
//  HyperCalcAppDelegate.m
//  HyperCalc
//
//  Created by Danny Draper on 31/05/2010.
//  Copyright CedeSoft Ltd 2010. All rights reserved.
//

#import "HyperCalcAppDelegate.h"
#import "HyperCalcViewController.h"

@implementation HyperCalcAppDelegate

@synthesize window;
@synthesize viewController;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    // Override point for customization after app launch    
    [window addSubview:viewController.view];
    [window makeKeyAndVisible];

	return YES;
}


- (void)dealloc {
    [viewController release];
    [window release];
    [super dealloc];
}


@end
