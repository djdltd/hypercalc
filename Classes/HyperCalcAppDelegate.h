//
//  HyperCalcAppDelegate.h
//  HyperCalc
//
//  Created by Danny Draper on 31/05/2010.
//  Copyright CedeSoft Ltd 2010. All rights reserved.
//

#import <UIKit/UIKit.h>

@class HyperCalcViewController;

@interface HyperCalcAppDelegate : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    HyperCalcViewController *viewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) IBOutlet HyperCalcViewController *viewController;

@end

