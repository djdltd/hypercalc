//
//  HyperCalcViewController.h
//  HyperCalc
//
//  Created by Danny Draper on 31/05/2010.
//  Copyright CedeSoft Ltd 2010. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ReversePolishNotation.h"
#import <AudioToolbox/AudioToolbox.h>


@interface HyperCalcViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
	NSArray	*listData;
	IBOutlet UILabel *currentCalctext;
	IBOutlet UILabel *currentExpressionlabel;
	IBOutlet UILabel *degradLabel;
	IBOutlet UITextView *txtViewHistory;
	IBOutlet UITableView *historyTable;
	IBOutlet UIButton *degradButton;
	IBOutlet UIButton *sinButton;
	IBOutlet UIButton *cosButton;
	IBOutlet UIButton *tanButton;
	IBOutlet UIButton *sinhButton;
	IBOutlet UIButton *coshButton;
	IBOutlet UIButton *tanhButton;
	IBOutlet UIButton *invButton;
	IBOutlet UIButton *soundButton;
	SystemSoundID _sscalcbuttonid;
	SystemSoundID _sscalcanswerid;
}

@property (retain, nonatomic) NSArray *listData;
@property (retain, nonatomic) UITableView *historyTable;
@property (retain, nonatomic) UILabel *currentCalctext;
@property (retain, nonatomic) UILabel *degradLabel;
@property (retain, nonatomic) UIButton *degradButton;

@property (retain, nonatomic) UIButton *sinButton;
@property (retain, nonatomic) UIButton *cosButton;
@property (retain, nonatomic) UIButton *tanButton;

@property (retain, nonatomic) UIButton *sinhButton;
@property (retain, nonatomic) UIButton *coshButton;
@property (retain, nonatomic) UIButton *tanhButton;

@property (retain, nonatomic) UIButton *invButton;
@property (retain, nonatomic) UIButton *soundButton;

@property (retain, nonatomic) UITextView *txtViewHistory;
@property (retain, nonatomic) UILabel *currentExpressionlabel;

NSMutableString *historyText;
NSMutableString *currentCalc;
NSMutableString *lastValue;

bool _currentNegative;
NSMutableString *_strcurrentnumber;
NSMutableString *_strcurrentoperator;
NSMutableString *_strfullexpression;
NSMutableString *_strdisplayexpression;
NSMutableString *_strlastkey;
bool _boperatorset;
bool _bcommandsdisabled;
bool _blastkeywasnumber;
bool _ballowbackspace;
bool _ballowequals;
bool _bfunctionjustperformed;
NSMutableString *_strlastnumberentered;
NSMutableString *_strlastoperator;
bool _bexpressionevaluated;
NSMutableString *_strbackupexpression;
NSMutableString *_strbackupdisplayexpression;
NSMutableArray *calcStack;
bool _busingradians;
NSMutableArray *_historyList;
NSIndexPath *_historyindexpath;
bool _busesounds;
bool _bfirstrun;

ReversePolishNotation *_reversepolishnotation;

- (IBAction)calcbuttonPressed:(id)sender;
- (IBAction)clearbuttonPressed:(id)sender;
- (IBAction)soundbuttonPressed:(id)sender;
- (IBAction)copybuttonPressed:(id)sender;
- (bool)isCommand:(NSString *)inputText;
- (bool)isEquals:(NSString *)inputText;
- (void) ToggleSign;
- (void) BackspaceNumber;
- (void) ClearEverything:(bool)includelastkey;
- (NSString *) GetExpression;
- (NSString *) GetDisplayExpression;
- (void) RefreshDisplay:(bool)addnumber;
- (bool)isChar:(UniChar)testChar:(NSString *)inputText;
- (NSString *) EvaluateExpression;
- (void) EvaluateFunction:(NSString *)functionOp;
- (void) ShowBackupExpression;
- (void) AddtoHistory;
- (void) AddhelptoHistory;



@end

