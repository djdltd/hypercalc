//
//  HyperCalcViewController.m
//  HyperCalc
//
//  Created by Danny Draper on 31/05/2010.
//  Copyright CedeSoft Ltd 2010. All rights reserved.
//

#import "HyperCalcViewController.h"

@implementation HyperCalcViewController
@synthesize currentCalctext;
@synthesize currentExpressionlabel;
@synthesize txtViewHistory;
@synthesize degradLabel;
@synthesize degradButton;
@synthesize historyTable;
@synthesize listData;
@synthesize sinButton;
@synthesize cosButton;
@synthesize tanButton;
@synthesize sinhButton;
@synthesize coshButton;
@synthesize tanhButton;
@synthesize invButton;
@synthesize soundButton;



- (bool)isCommand:(NSString *)inputText
{
	if ([inputText isEqualToString:@"2nd"] == true) {
		return true;
	}
	
	UniChar singlechar = [inputText characterAtIndex:0];
	
	if (singlechar >= '0' && singlechar <='9') {
		return false;
	} else {
		if (singlechar == '.') {
			return false;
		} else {
			return true;
		}
	}
}

- (void) ToggleSign
{
	if (_currentNegative == true) {
		_currentNegative = false;
	} else {
		_currentNegative = true;
	}
}

- (NSString *) GetExpression
{
	NSMutableString *result;	
	result = [NSMutableString new];
	
	if (_currentNegative == true)
	{
		[result appendString:_strfullexpression];
		[result appendString:@" ~ "];
		[result appendString:_strcurrentnumber];
		[result appendString:@" "];
		[result appendString:_strcurrentoperator];
		[result appendString:@" "];
	}
	else 
	{
		[result appendString:_strfullexpression];
		[result appendString:_strcurrentnumber];
		[result appendString:@" "];
		[result appendString:_strcurrentoperator];
		[result appendString:@" "];		
	}
	
	[result setString:[result stringByReplacingOccurrencesOfString:@"  " withString:@" "]];
	//result = [result stringByReplacingOccurrencesOfString:@"  " withString:@" "];
	
	return result;
}

- (NSString *) GetDisplayExpression
{
	NSMutableString *result;	
	result = [NSMutableString new];

	if (_currentNegative == true)
	{
		[result appendString:_strdisplayexpression];
		[result appendString:@" - "];
		[result appendString:_strcurrentnumber];
		[result appendString:@" "];
		[result appendString:_strcurrentoperator];
		[result appendString:@" "];
	}
	else 
	{
		[result appendString:_strdisplayexpression];
		[result appendString:_strcurrentnumber];
		[result appendString:@" "];
		[result appendString:_strcurrentoperator];
		[result appendString:@" "];		
	}

	[result setString:[result stringByReplacingOccurrencesOfString:@"  " withString:@" "]];
	
	return result;
}

- (void) ShowBackupExpression
{
	currentExpressionlabel.text = _strbackupdisplayexpression;
}

- (void) BackspaceNumber
{
	NSString *backspacestring;
	backspacestring = [NSString new];
	
	if ([_strcurrentnumber length] > 1) {
	
		backspacestring = [_strcurrentnumber substringToIndex:[_strcurrentnumber length]-1];
	
		[_strcurrentnumber setString:backspacestring];
	} else {
		[_strcurrentnumber setString:@""];
		
	}
	
	[_strlastnumberentered setString:_strcurrentnumber];
	
}

- (void) ClearEverything:(bool)includelastkey
{
	if (includelastkey == true) {
		[_strcurrentnumber setString:@""];
		[_strlastnumberentered setString:@""];
		[_strlastkey setString:@""];
	}
	
	_currentNegative = false;
	[_strcurrentoperator setString:@""];
	[_strfullexpression setString:@""];
	[_strdisplayexpression setString:@""];
	
	_boperatorset = false;
	[self RefreshDisplay:false];
	
	[_strlastoperator setString:@""];
	_bexpressionevaluated = false;
	
	currentCalctext.text = @"0";
	currentExpressionlabel.text = @"0";
	_ballowequals = false;
	
	[_reversepolishnotation release];
	_reversepolishnotation = [ReversePolishNotation new];
	[_reversepolishnotation setRadians:_busingradians];
}

- (void) RefreshDisplay:(bool)addnumber
{
	NSMutableString *currentcalctext;
	currentcalctext = [NSMutableString new];
	
	if (addnumber == true)
	{
		[_strcurrentnumber appendString:_strlastkey];
		[_strlastnumberentered setString:_strcurrentnumber];
	}
	
	if (_currentNegative == true)
	{
		[currentcalctext appendString:@"-"];
		if ([_strcurrentnumber length] > 0) {
			[currentcalctext appendString:_strcurrentnumber];
			currentCalctext.text = currentcalctext;
		} else {
			currentCalctext.text = @"0";
		}
	}
	else 
	{
		if ([_strcurrentnumber length] > 0) {
			[currentcalctext setString:_strcurrentnumber];
			currentCalctext.text = currentcalctext;
		} else {
			currentCalctext.text = @"0";
		}
	}
	
	currentExpressionlabel.text = [self GetDisplayExpression];
	NSLog (@"DISPLAYREFRESH: Full Expression: %@", _strfullexpression);

	//[currentcalctext release];
	
}

- (void) AddhelptoHistory
{

	[_historyList addObject:@"New calculations will be added to this list."];
	[_historyList addObject:@"Tap any result in this list to use the number in your current"];
	[_historyList addObject:@"calculation."];
	[_historyList addObject:@""];
	[_historyList addObject:@"The buttons on the right perform the following functions:"];
	[_historyList addObject:@"  Clear   - Clear this list of all previous calculations."];
	[_historyList addObject:@"  Snd off - Press to toggle the beep sounds on & off."];
	[_historyList addObject:@"  Copy    - Copy this entire list to the clipboard."];
	
	self.listData = _historyList;
	[historyTable reloadData];
}

- (void) AddtoHistory
{
	if (_bfirstrun == true) {
		[_historyList removeAllObjects];
		self.listData = _historyList;
		[historyTable reloadData];
		_bfirstrun = false;
	}
	
	[_historyList addObject:currentExpressionlabel.text];
	
	NSMutableString *answerstring;
	answerstring = [NSMutableString new];
	[answerstring appendString:@"   =  "];
	[answerstring appendString:currentCalctext.text];
	
	[_historyList addObject:answerstring];
	
	self.listData = _historyList;
	[historyTable reloadData];
	
	
	if ([_historyList count] > 1) {				
		
		
		//indexpath = [[NSIndexPath alloc] initWithIndex:0];
				
		_historyindexpath = [_historyindexpath indexPathByAddingIndex:[_historyList count]-1];
		[historyTable scrollToRowAtIndexPath:_historyindexpath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
		
		_historyindexpath = [_historyindexpath indexPathByRemovingLastIndex];
	}
	
	[answerstring release];
}

- (bool)isEquals:(NSString *)inputText
{
	UniChar singlechar = [inputText characterAtIndex:0];
	
	if (singlechar == '=') {
		return true;
	} else {
		return false;
	}
}

- (bool)isChar:(UniChar)testChar:(NSString *)inputText
{
	
	UniChar singlechar = [inputText characterAtIndex:0];
	
	if (singlechar == testChar) {
		return true;
	} else {
		return false;
	}
}

- (NSString *) EvaluateExpression
{
	double result = 0.0;
	NSNumber *numresult;
	
	NSString *expression;
	expression = [NSString new];
	expression = [self GetExpression];
	
	NSCharacterSet *charset;
	charset = [NSCharacterSet new];
	charset = [NSCharacterSet whitespaceCharacterSet];
	
	expression = [expression stringByTrimmingCharactersInSet:charset];
	expression = [expression stringByReplacingOccurrencesOfString:@"  " withString:@" "];
	
	
	[_reversepolishnotation MakeRPN:expression];
	result = [_reversepolishnotation Evaluate];
	
	numresult = [[NSNumber alloc] initWithDouble:result];
	
	NSLog (@"Expression evaluated, answer: %f", result);
	
	return [numresult stringValue];
	
	
}

- (void) EvaluateFunction:(NSString *)functionOp
{
	double dblresult = 0.0;
	NSNumber *numresult;
	NSMutableString *currentNum;
	currentNum = [NSMutableString new];
	
	[currentNum setString:currentCalctext.text];
	
		
	[_strcurrentnumber setString:@""];
	//_currentNegative = false;
	[_strcurrentoperator setString:@""];
	[_strfullexpression setString:@""];
	[_strdisplayexpression setString:@""];
	[_strlastkey setString:@""];
	_boperatorset = false;
	//[self RefreshDisplay:false];
	[_strlastnumberentered setString:@""];
	[_strlastoperator setString:@""];
	_bexpressionevaluated = false;
	
	//currentCalctext.text = @"0";
	//currentExpressionlabel.text = @"0";

	NSMutableString *result;
	result = [NSMutableString new];
	
	if (_currentNegative == true)
	{
		[result appendString:functionOp];
		[result appendString:@" ( "];
		[result appendString:@" ~ "];
		[result appendString:currentNum];
		[result appendString:@" )"];
	}
	else 
	{
		[result appendString:functionOp];
		[result appendString:@" ( "];
		[result appendString:currentNum];
		[result appendString:@" )"];
	}
	
	[_strfullexpression setString:result];
	[_strdisplayexpression setString:[result stringByReplacingOccurrencesOfString:@"~" withString:@"-"]];

	[_reversepolishnotation MakeRPN:result];
	dblresult = [_reversepolishnotation Evaluate];
	
	numresult = [[NSNumber alloc] initWithDouble:dblresult];
	currentCalctext.text = [numresult stringValue];
	
	currentExpressionlabel.text = _strdisplayexpression;
	_bfunctionjustperformed = true;
	
	[numresult release];
	
	[self AddtoHistory];
}

- (IBAction)copybuttonPressed:(id)sender
{
	UIPasteboard *appPasteBoard = [UIPasteboard generalPasteboard];
	
	//appPasteBoard.persistent = YES;
	
	NSMutableString *copytext;
	copytext = [NSMutableString new];
	NSString *currenttext;
	
	int i;
	
	for (i=0;i<[_historyList count];++i)
	{
		currenttext = [_historyList objectAtIndex:i];
		[copytext appendString:currenttext];
		[copytext appendString:@"\n"];
	}
	
	[appPasteBoard setString:copytext];
	
	[copytext release];
	
	//NSString *message = [[NSString alloc] initWithFormat:@"You selected %@", rowValue];
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Copied to Clipboard" message:@"History has been copied to the Clipboard" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
	[alert show];
	
	[alert release];
	
}

- (IBAction)soundbuttonPressed:(id)sender
{
	if (_busesounds == true) {
		_busesounds = false;
		[soundButton setTitle:@"Snd on" forState:UIControlStateNormal];
	} 
	else
	{
		_busesounds = true;
		[soundButton setTitle:@"Snd off" forState:UIControlStateNormal];
	}
}

- (IBAction)clearbuttonPressed:(id)sender
{
	[_historyList removeAllObjects];
	self.listData = _historyList;
	[historyTable reloadData];
}

- (IBAction)calcbuttonPressed:(id)sender
{
	//currentCalctext.text = @"Hello.";
	NSString *keyinputstring = [sender titleForState:UIControlStateNormal];
	[_strlastkey setString:keyinputstring];
	
	if ([self isEquals:keyinputstring] == false) 
	{
		_bexpressionevaluated = false;
	}
	
	
	if([self isCommand:keyinputstring] == true)
	{
		if ([keyinputstring isEqualToString:@"CE"] == true) {			
			[self ClearEverything:true];
			
		}
		
		if (_bcommandsdisabled == false) {
				
		if ([keyinputstring isEqualToString:@"±"] == true) {
			
			[self ToggleSign];
			[self RefreshDisplay:false];
			//NSLog (@"Sign button pressed!");
			
		}
		
		if ([keyinputstring isEqualToString:@"="] == true) {
			
			if (_ballowequals == true) {
				
				if (_bexpressionevaluated == true)
				{
					[_strfullexpression appendString:_strlastnumberentered];
					[_strfullexpression appendString:@" "];
					[_strfullexpression appendString:_strlastoperator];
					[_strfullexpression appendString:@" "];
				
					[_strdisplayexpression appendString:_strlastnumberentered];
					[_strdisplayexpression appendString:@" "];
					[_strdisplayexpression appendString:_strlastoperator];
					[_strdisplayexpression appendString:@" "];
				
					[self RefreshDisplay:false];
				}
			
				currentCalctext.text = [self EvaluateExpression];
				_bexpressionevaluated = true;
				[self AddtoHistory];

				[_reversepolishnotation release];
				_reversepolishnotation = [ReversePolishNotation new];
				[_reversepolishnotation setRadians:_busingradians];
			}
			
		}
		
		if ([keyinputstring isEqualToString:@"Mod"] == true) {
			_bfunctionjustperformed = false;
			_boperatorset = true;
			_ballowequals = true;
			[_strcurrentoperator setString:@"mod"];
			[_strlastoperator setString:_strcurrentoperator];
			[self RefreshDisplay:false];
			currentCalctext.text = [self EvaluateExpression];
		}
		
		if ([keyinputstring isEqualToString:@"x^y"] == true) {
			_bfunctionjustperformed = false;
			_boperatorset = true;
			_ballowequals = true;
			[_strcurrentoperator setString:@"^"];
			[_strlastoperator setString:_strcurrentoperator];
			[self RefreshDisplay:false];
			currentCalctext.text = [self EvaluateExpression];
		}
			
		if ([keyinputstring isEqualToString:@"+"] == true) {
			_bfunctionjustperformed = false;
			_boperatorset = true;
			_ballowequals = true;
			[_strcurrentoperator setString:@"+"];
			[_strlastoperator setString:_strcurrentoperator];
			[self RefreshDisplay:false];
			currentCalctext.text = [self EvaluateExpression];
		}
		
		if ([keyinputstring isEqualToString:@"-"] == true) {
			_bfunctionjustperformed = false;
			_boperatorset = true;
			_ballowequals = true;
			[_strcurrentoperator setString:@"-"];
			[_strlastoperator setString:_strcurrentoperator];
			[self RefreshDisplay:false];
			currentCalctext.text = [self EvaluateExpression];
		}
		
		if ([keyinputstring isEqualToString:@"/"] == true) {
			_bfunctionjustperformed = false;
			_boperatorset = true;
			_ballowequals = true;
			[_strcurrentoperator setString:@"/"];
			[_strlastoperator setString:_strcurrentoperator];
			[self RefreshDisplay:false];
			currentCalctext.text = [self EvaluateExpression];
		}
		
		if ([keyinputstring isEqualToString:@"*"] == true) {
			_bfunctionjustperformed = false;
			_boperatorset = true;
			_ballowequals = true;
			[_strcurrentoperator setString:@"*"];
			[_strlastoperator setString:_strcurrentoperator];
			[self RefreshDisplay:false];
			currentCalctext.text = [self EvaluateExpression];
		}
		
		if ([keyinputstring isEqualToString:@"("] == true) {
			_ballowequals = false;
			if (_boperatorset == true)
			{
				[_strfullexpression setString:[self GetExpression]];
				[_strfullexpression appendString:@" ( "];
				[_strdisplayexpression setString:[self GetDisplayExpression]];
				[_strdisplayexpression appendString:@" ( "];
				[_strcurrentnumber setString:@""];
				[_strcurrentoperator setString:@""];
				_currentNegative = false;
				_boperatorset = false;
				[self RefreshDisplay:false];
			}
			
		}
		
		if ([keyinputstring isEqualToString:@")"] == true) {
			_ballowequals = true;
			[_strfullexpression setString:[self GetExpression]];
			[_strfullexpression appendString:@") "];
			[_strdisplayexpression setString:[self GetDisplayExpression]];
			[_strdisplayexpression appendString:@") "];
			[_strcurrentnumber setString:@""];
			[_strcurrentoperator setString:@""];
			_currentNegative = false;
			_boperatorset = false;
			[self RefreshDisplay:false];
		}
		
		if ([keyinputstring isEqualToString:@"C"] == true) {
			
			if (_blastkeywasnumber == true) {
				NSLog (@"Backed up expression: %@", _strbackupexpression);
			
				_currentNegative = false;
				[_strcurrentoperator setString:@""];
				[_strcurrentnumber setString:@""];
				_boperatorset = false;
				[self RefreshDisplay:false];
				//[_strlastnumberentered setString:@""];
				//[_strlastoperator setString:@""];
				_bexpressionevaluated = false;
			
				[_strfullexpression setString:_strbackupexpression];
				[_strdisplayexpression setString:_strbackupdisplayexpression];
			
				[self RefreshDisplay:false];
			
				currentCalctext.text = @"0";
				_bcommandsdisabled = true;
			}
		} else {
			_blastkeywasnumber = false;
			[_strbackupexpression setString:[self GetExpression]];
			[_strbackupdisplayexpression setString:[self GetDisplayExpression]];
			NSLog(@"Expression backed up: %@", _strbackupexpression);

		}
			
		if ([keyinputstring isEqualToString:@"Bksp"] == true) {
			
			if (_ballowbackspace == true) {
				[self BackspaceNumber];
				[self RefreshDisplay:false];
			}
			
		} else {
			_ballowbackspace = false;
		}
		
		if ([keyinputstring isEqualToString:@"sin"] == true) {
			[self EvaluateFunction:@"sin"];
		}
		
		if ([keyinputstring isEqualToString:@"cos"] == true) {
			[self EvaluateFunction:@"cos"];
		}

		if ([keyinputstring isEqualToString:@"tan"] == true) {
			[self EvaluateFunction:@"tan"];
		}
		
		if ([keyinputstring isEqualToString:@"sinh"] == true) {
			[self EvaluateFunction:@"sinh"];
		}
		
		if ([keyinputstring isEqualToString:@"cosh"] == true) {
			[self EvaluateFunction:@"cosh"];
		}
		
		if ([keyinputstring isEqualToString:@"tanh"] == true) {
			[self EvaluateFunction:@"tanh"];
		}
		
		if ([keyinputstring isEqualToString:@"asin"] == true) {
			[self EvaluateFunction:@"asin"];
		}
			
		if ([keyinputstring isEqualToString:@"acos"] == true) {
			[self EvaluateFunction:@"acos"];
		}
			
		if ([keyinputstring isEqualToString:@"atan"] == true) {
			[self EvaluateFunction:@"atan"];
		}
			
		if ([keyinputstring isEqualToString:@"asinh"] == true) {
			[self EvaluateFunction:@"asinh"];
		}
			
		if ([keyinputstring isEqualToString:@"acosh"] == true) {
			[self EvaluateFunction:@"acosh"];
		}
			
		if ([keyinputstring isEqualToString:@"atanh"] == true) {
			[self EvaluateFunction:@"atanh"];
		}
			
		if ([keyinputstring isEqualToString:@"log"] == true) {
			[self EvaluateFunction:@"log"];
		}
		
		if ([keyinputstring isEqualToString:@"log2"] == true) {
			[self EvaluateFunction:@"log2"];
		}
		
		if ([keyinputstring isEqualToString:@"Cbrt"] == true) {
			[self EvaluateFunction:@"cbrt"];
		}
			
		if ([keyinputstring isEqualToString:@"Sqrt"] == true) {
			[self EvaluateFunction:@"sqrt"];
		}
		
		if ([keyinputstring isEqualToString:@"x2"] == true) {
			[self EvaluateFunction:@"x2"];
		}
			
		if ([keyinputstring isEqualToString:@"%"] == true) {
			[self EvaluateFunction:@"%"];
		}
			
		if ([keyinputstring isEqualToString:@"Rad"] == true) {
			degradLabel.text = @"Rad";
			//((UIButton)sender).text = @"Deg";
			[degradButton setTitle:@"Deg" forState:UIControlStateNormal];
			_busingradians = true;
			[_reversepolishnotation setRadians:_busingradians];
		}
			
			
		if ([keyinputstring isEqualToString:@"Deg"] == true) {
			degradLabel.text = @"Deg";
			//((UIButton)sender).text = @"Deg";
			[degradButton setTitle:@"Rad" forState:UIControlStateNormal];
			_busingradians = false;
			[_reversepolishnotation setRadians:_busingradians];
		}
			
		if ([keyinputstring isEqualToString:@"Inv"] == true) {
			
			//((UIButton)sender).text = @"Deg";
			[invButton setTitle:@"Std" forState:UIControlStateNormal];
		
			[sinButton setTitle:@"asin" forState:UIControlStateNormal];
			[cosButton setTitle:@"acos" forState:UIControlStateNormal];
			[tanButton setTitle:@"atan" forState:UIControlStateNormal];
			
			[sinhButton setTitle:@"asinh" forState:UIControlStateNormal];
			[coshButton setTitle:@"acosh" forState:UIControlStateNormal];
			[tanhButton setTitle:@"atanh" forState:UIControlStateNormal];
			
		}

			
		if ([keyinputstring isEqualToString:@"Std"] == true) {
				
			//((UIButton)sender).text = @"Deg";
			[invButton setTitle:@"Inv" forState:UIControlStateNormal];
				
			[sinButton setTitle:@"sin" forState:UIControlStateNormal];
			[cosButton setTitle:@"cos" forState:UIControlStateNormal];
			[tanButton setTitle:@"tan" forState:UIControlStateNormal];
				
			[sinhButton setTitle:@"sinh" forState:UIControlStateNormal];
			[coshButton setTitle:@"cosh" forState:UIControlStateNormal];
			[tanhButton setTitle:@"tanh" forState:UIControlStateNormal];
				
		}
			
			
		if ([keyinputstring isEqualToString:@"Std"] == true) {
				
			//((UIButton)sender).text = @"Deg";
			[invButton setTitle:@"Inv" forState:UIControlStateNormal];
				
			[sinButton setTitle:@"sin" forState:UIControlStateNormal];
			[cosButton setTitle:@"cos" forState:UIControlStateNormal];
			[tanButton setTitle:@"tan" forState:UIControlStateNormal];
				
			[sinhButton setTitle:@"sinh" forState:UIControlStateNormal];
			[coshButton setTitle:@"cosh" forState:UIControlStateNormal];
			[tanhButton setTitle:@"tanh" forState:UIControlStateNormal];
				
		}
			
		if ([keyinputstring isEqualToString:@"pi"] == true) {
			NSNumber *constValue;
			constValue = [[NSNumber alloc] initWithDouble:M_PI];
			
			currentCalctext.text = [constValue stringValue];
			[_strcurrentnumber setString:[constValue stringValue]];
			
			[constValue release];
		}
			
			
		if ([keyinputstring isEqualToString:@"e"] == true) {
			NSNumber *constValue;
			constValue = [[NSNumber alloc] initWithDouble:M_E];
				
			currentCalctext.text = [constValue stringValue];
			[_strcurrentnumber setString:[constValue stringValue]];
			
			[constValue release];
		}
		//[rpn doSomething:(int) 1];
		//[_reversepolishnotation doSomething:3];
		
		//[_reversepolishnotation stackTest];
		
		//[_reversepolishnotation stringSplitTest];
		
		//[_reversepolishnotation MakeRPN:@"123.127 + 45.3 - 10 + ( 3 + 2 )"];
		
		//double myanswer;
		//myanswer = [_reversepolishnotation Evaluate];
		
		//NSLog (@"The answer was: %f", myanswer);
		
		
		//[_reversepolishnotation Doubletest];
		//NSNumber *mynumber;
		//mynumber = [[NSNumber alloc] initWithDouble:25.4];
		//mynumber = [[NSNumber alloc] initWithDouble:35.4];
		//mynumber = [[NSNumber alloc] initWithDouble:45.4];
		//mynumber = [[NSNumber alloc] initWithDouble:55.4];
		
		//NSLog (@"NSNumber value is: %@", mynumber);
		
		//NSLog(@"Hello");
		//[historyText appendString:currentCalc];
		//[historyText appendString:@"\n"];
		//[historyText appendString:keyinputstring];
		//[historyText appendString:@"\t"];
		//[historyText appendString:@"\n"];
		
		
		//txtViewHistory.text = historyText;
		
		//[currentCalc setString:@""];
		//currentCalctext.text = currentCalc;
		
		
		//[calcStack addObject:currentCalc];
		//[calcStack addOb
		//[lastValue 
			
		} // _bcommandsdisabled
	} else {
		
		//[currentCalc appendString:@"F"];
		_bcommandsdisabled = false; // This ensures you can't type an operator after pressing C
		_blastkeywasnumber = true;
		_ballowbackspace = true;
		
		if (_bfunctionjustperformed == true)
		{
			[self ClearEverything:false];
			_bfunctionjustperformed = false;
		}
		
		if (_boperatorset == true)
		{
			NSLog (@"Number Pressed: Full Expression: %@", _strfullexpression);
			
			[_strfullexpression setString:[self GetExpression]];
			[_strdisplayexpression setString:[self GetDisplayExpression]];
			
			[_strcurrentnumber setString:@""];
			[_strcurrentoperator setString:@""];
			_currentNegative = false;
			_boperatorset = false;
			[self RefreshDisplay:true];
			

		}
		else 
		{
			[self RefreshDisplay:true];
		}

		
		//[currentCalc appendString:keyinputstring];
		//currentCalctext.text = currentCalc;
	}
	
	//currentCalctext.text = title;
	
	//if ([historyText length]>0) {
	
		//[txtViewHistory scrollRangeToVisible:NSMakeRange([historyText length]-1, 1)];
	//}
	if (_busesounds == true) {
		if ([keyinputstring isEqualToString:@"="] == true) {
			AudioServicesPlaySystemSound(_sscalcanswerid);
		} else {
			AudioServicesPlaySystemSound(_sscalcbuttonid);
		}
	}
	
	
}

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/



// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    
	_historyindexpath = [NSIndexPath new];
	_historyindexpath = [_historyindexpath indexPathByAddingIndex:0];
	
	historyText = [[NSMutableString alloc] initWithCapacity:10];
	currentCalc = [[NSMutableString alloc] initWithCapacity:10];
	calcStack = [[NSMutableArray alloc] initWithCapacity:10];
	lastValue = [[NSMutableString alloc] initWithCapacity:10];
	_historyList = [NSMutableArray new];
	
	_reversepolishnotation = [[ReversePolishNotation alloc] init];
	_strcurrentnumber = [NSMutableString new];
	
	_strcurrentoperator = [NSMutableString new];
	_strfullexpression = [NSMutableString new];
	_strdisplayexpression = [NSMutableString new];
	_strlastkey = [NSMutableString new];
	_boperatorset = false;
	_strlastnumberentered = [NSMutableString new];
	_strlastoperator = [NSMutableString new];
	_strbackupexpression = [NSMutableString new];
	_strbackupdisplayexpression = [NSMutableString new];
	_bexpressionevaluated = false;
	_ballowequals = false;
	_busingradians = false;
	[_reversepolishnotation setRadians:_busingradians];
	
	//NSArray *array = [[NSArray alloc] initWithObjects:@"Test 1", @"Test 2", @"Test 3", @"Test 4", @"Test 5", nil];
	self.listData = _historyList;
	
	//[array release];
	
	// Load the sounds
	NSString *sndbuttonPath = [[NSBundle mainBundle] pathForResource:@"calcbuttonbeep" ofType:@"wav" inDirectory:@"/"];
	NSLog (@"Button Sound path: %@", sndbuttonPath);
	CFURLRef sndbuttonURL = (CFURLRef)[[NSURL alloc] initFileURLWithPath:sndbuttonPath];
	AudioServicesCreateSystemSoundID(sndbuttonURL, &_sscalcbuttonid);

	NSString *sndanswerPath = [[NSBundle mainBundle] pathForResource:@"calcanswer" ofType:@"wav" inDirectory:@"/"];
	NSLog (@"Answer Sound path: %@", sndanswerPath);	
	CFURLRef sndanswerURL = (CFURLRef)[[NSURL alloc] initFileURLWithPath:sndanswerPath];	
	AudioServicesCreateSystemSoundID(sndanswerURL, &_sscalcanswerid);
	
	_busesounds = true;
	_bfirstrun = true;
	
	[self AddhelptoHistory];
	
	[super viewDidLoad];
}



// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return YES;
}

- (void)didReceiveMemoryWarning {
	// Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
	
	// Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	// Release any retained subviews of the main view.
	// e.g. self.myOutlet = nil;
}


- (void)dealloc {
	[currentCalc release];
	[historyText release];
	[_historyindexpath release];
    [super dealloc];
}

#pragma mark -
#pragma mark Table View Datasource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
	return [self.listData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString *SimpleTableIdentifier = @"SimpleTableIdentifier";
	
	UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:SimpleTableIdentifier];
	if (cell == nil) {
		cell = [[[UITableViewCell alloc] initWithFrame:CGRectZero reuseIdentifier:SimpleTableIdentifier] autorelease];
	}
	
	NSUInteger row = [indexPath row];
	cell.text = [listData objectAtIndex:row];
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
	NSUInteger row = [indexPath row];
	NSString *rowValue = [listData objectAtIndex:row];
	

	NSString *filtered = [NSString new];
	NSString *filtereddisplay = [NSString new];
	
	if ([rowValue length] > 0) {
	
		if ([rowValue rangeOfString:@"="].location != NSNotFound) {
			//[alert show];
			filtered = [rowValue stringByReplacingOccurrencesOfString:@" " withString:@""];
			filtered = [filtered stringByReplacingOccurrencesOfString:@"=" withString:@""];
			filtereddisplay = [rowValue stringByReplacingOccurrencesOfString:@" " withString:@""];
			filtereddisplay = [filtereddisplay stringByReplacingOccurrencesOfString:@"=" withString:@""];
			
			if (_boperatorset == false)
			{
				if ([filtered rangeOfString:@"-"].location != NSNotFound) {
					_currentNegative = true;
					filtered = [filtered stringByReplacingOccurrencesOfString:@"-" withString:@""];
					filtereddisplay = [filtereddisplay stringByReplacingOccurrencesOfString:@"-" withString:@""];
				} else {
					_currentNegative = false;
				}
			} else {
				if ([filtered rangeOfString:@"-"].location != NSNotFound) {
					filtered = [filtered stringByReplacingOccurrencesOfString:@"-" withString:@"~ "];
					filtereddisplay = [filtereddisplay stringByReplacingOccurrencesOfString:@"-" withString:@"- "];
				} 			
			}

			
			_bcommandsdisabled = false; // This ensures you can't type an operator after pressing C
			_blastkeywasnumber = true;
			_ballowbackspace = true;
			
			if (_bfunctionjustperformed == true)
			{
				[self ClearEverything:false];
				_bfunctionjustperformed = false;
			}
			
			if (_boperatorset == true)
			{
				NSLog (@"Number Pressed: Full Expression: %@", _strfullexpression);
				
				[_strfullexpression setString:[self GetExpression]];
				[_strdisplayexpression setString:[self GetDisplayExpression]];
				[_strfullexpression appendString:filtered];
				[_strdisplayexpression appendString:filtereddisplay];
				
				
				[_strcurrentnumber setString:@""];
				[_strcurrentoperator setString:@""];
				_currentNegative = false;
				_boperatorset = false;
				[self RefreshDisplay:false];
				
				currentCalctext.text = filtereddisplay;
			}
			else 
			{
				//currentCalctext.text = filtered;
				[_strcurrentnumber setString:filtered];
				[_strlastnumberentered setString:filtered];
				[self RefreshDisplay:false];
			}
			
			//[self RefreshDisplay:false];
		} 
	}
	
	//[filtered release];
	//[message release];
	//[alert release];
}

@end
